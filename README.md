# timestampcli

A utility for better understanding timestamps from the command line.

## Usage

It just reads stdin and outputs to stdout, so here are a few ways you could use it:

```
# Printing to terminal
echo 1234567890 | timestampcli

# Before reading a file
cat logs.json | timestampcli | less

# Converting timestamps in a CSV
cat input.csv | timestampcli > output.csv
```

## Origin

When investigating issues I usually use the command line a lot for searching and transforming logs or data dumps.
However, if the key to understanding the problem is a date that appears in an epoch timestamp format I start doing a lot of round trips to a timestamp conversion website.
To save myself some time, I thought it would be nice to have a utility that I could throw in between my process' pipes to render timestamps in a more human-readable format.

