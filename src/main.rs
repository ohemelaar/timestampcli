use chrono::{TimeZone, Utc};
use clap::{crate_version, App, Arg};
use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

extern crate clap;
use regex::Regex;

fn main() {
    let matches = App::new("timestampcli")
        .version(crate_version!())
        .author("Oscar Hemelaar <ohemelaar@e.email>")
        .about("Converts numeric timestamps to more human-readable formats.")
        .arg(Arg::with_name("filename").value_name("FILE").help(
            "File to show timestamp conversion for. If omitted standard input is used instead.",
        ))
        .get_matches();
    match matches.value_of("filename") {
        Some(filename) => {
            let file = File::open(filename).expect(&format!("File not found: {}", filename));
            for line in BufReader::new(file).lines() {
                println!("{}", timestampify_text(&line.unwrap()));
            }
        }
        None => {
            let stdin = io::stdin();
            for line in stdin.lock().lines() {
                println!("{}", timestampify_text(&line.unwrap()));
            }
        }
    }
}

fn timestampify_text(input: &String) -> String {
    let timestamp_re = Regex::new(r"(^|[^0-9])(?P<t>1[0-9]{9}([0-9]{3}){0,3})([^0-9]|$)").unwrap();
    let captures = timestamp_re.captures_iter(input);
    let mut result = "".to_string();
    let mut prev_end = 0;
    for capture in captures {
        let ts_match = capture.name("t").unwrap();
        let seconds = ts_match.as_str()[0..10].parse::<i64>().unwrap();
        let nanoseconds = parse_nanoseconds(&ts_match.as_str()[10..]);
        let date_time = Utc.timestamp(seconds, nanoseconds);
        result.push_str(&input[prev_end..ts_match.start()]);
        result.push_str(&date_time.to_rfc3339());
        prev_end = ts_match.end();
    }
    result.push_str(&input[prev_end..input.len()]);
    result
}

fn parse_nanoseconds(input: &str) -> u32 {
    match input.len() {
        0 => 0,
        3 => input.parse::<u32>().unwrap() * 1_000_000,
        6 => input.parse::<u32>().unwrap() * 1_000,
        9 => input.parse::<u32>().unwrap(),
        _ => todo!("I don't know yet how this error should be handled"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_dev() {
        let input = "lksfdj> 1234567890 <slkfj slfj ' 1234567890 ' sfljk";
        println!("{}", input);
        println!("{}", timestampify_text(&input.to_string()));
    }
}
